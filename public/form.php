<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-wedth,initial-scale=1.0">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="style.css">
<title>Задание 4</title>
</head>
<body class="p-0 m-0 color1 text-black">
<header class="container-fluid  p-0 m-0  color2 ">
<div class="container">
<span style="font-size: 30px" >
	Задание 4
</span>
</div>
</header>
<h1>Форма</h1>
<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $mess) {
	print($mess);
  }
  print('</div><br/><br/>');
}
?>
            <form action="" method="POST">
              <br/>Имя:<br/>
              <input type="text" name="name" <?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>"><br/>
              <br/>email:<br/>
              <input type="email" name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"><br/>
              <br/>Дата рождения:<br/>
                <input name="date" <?php if ($errors['date']) {print 'class="error"';} ?> value="<?php print $values['date']; ?>" type="text"/>
              <br/>
                <br/><a id="gender"></a>Пол:<br/>
                  <input type="radio" name="gender" value="male" <?php if ($values['gender'] == 'male') {print 'checked="checked"';} ?>/>Мужской<br/>
                  <input type="radio" name="gender" value="female" <?php if ($values['gender'] == 'female') {print 'checked="checked"';} ?>/>Женский<br/>
              <br/>Кол-во конечностей:<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="1" <?php if ($values['limb'] == '1') {print 'checked="checked"';} ?> />1<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="2" <?php if ($values['limb'] == '2') {print 'checked="checked"';} ?> />2<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="3" <?php if ($values['limb'] == '3') {print 'checked="checked"';} ?> />3<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value="4" <?php if ($values['limb'] == '4') {print 'checked="checked"';} ?> />4<br/>
                <input type="radio" name="limb" <?php if ($errors['limb']) {print 'class="error"';} ?> value=">4" <?php if ($values['limb'] == '>4') {print 'checked="checked"';} ?> />>4<br/>    
              <br/>Сверхспособности:<br/>
                <input type="checkbox" name="super1" value="бессмертие" <?php if ($values['super1'] != '') {print 'checked="checked"';} ?> />Бессмертие<br/>
                <input type="checkbox" name="super2" value="прохождение сквозь стены" <?php if ($values['super2'] != '') {print 'checked="checked"';} ?> />Прохождение сквозь стены<br/>
                <input type="checkbox" name="super3" value="левитация" <?php if ($values['super3'] != '') {print 'checked="checked"';} ?> />Левитация<br/>
                <br/>Биография:<br/>
                <textarea name="message" value="<?php print $values['message']; ?>">Расскажите о себе</textarea>
                <input type="checkbox" name="check" value="+" <?php if ($values['check'] != '') {print 'checked="checked"';} ?>/>С контрактом ознакомлен<br/>
              <input type="submit" name="send" value="Отправить" class="submit"/>        
            </form>

<footer class="footer color2 ">
<span>
(c) Малхасян Карен
</span>
</footer>
</body>
</html>
